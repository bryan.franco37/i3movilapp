﻿using System.Collections.Generic;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class Group
    {
        public Group()
        {
            Question = new HashSet<Question>();
        }

        public int Id { get; set; }
        public int? SubSectionId { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
        public bool? IsQuestion { get; set; }

        public virtual SubSection SubSection { get; set; }
        public virtual ICollection<Question> Question { get; set; }
    }
}
