﻿using System.Collections.Generic;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class Option
    {
        public Option()
        {
            QuestionOption = new HashSet<QuestionOption>();
            ResponseQuestion = new HashSet<ResponseQuestion>();
        }

        public int Id { get; set; }
        public string Statement { get; set; }

        public virtual ICollection<QuestionOption> QuestionOption { get; set; }
        public virtual ICollection<ResponseQuestion> ResponseQuestion { get; set; }
    }
}
