﻿using System.Collections.Generic;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class Question
    {
        public Question()
        {
            InverseFatherQuestion = new HashSet<Question>();
            QuestionOption = new HashSet<QuestionOption>();
            QuestionRequirement = new HashSet<QuestionRequirement>();
            ResponseImage = new HashSet<ResponseImage>();
            ResponseQuestion = new HashSet<ResponseQuestion>();
        }

        public int Id { get; set; }
        public int? GroupId { get; set; }
        public int? FatherQuestionId { get; set; }
        public int? Number { get; set; }
        public string Statement { get; set; }

        public virtual Question FatherQuestion { get; set; }
        public virtual Group Group { get; set; }
        public virtual ICollection<Question> InverseFatherQuestion { get; set; }
        public virtual ICollection<QuestionOption> QuestionOption { get; set; }
        public virtual ICollection<QuestionRequirement> QuestionRequirement { get; set; }
        public virtual ICollection<ResponseImage> ResponseImage { get; set; }
        public virtual ICollection<ResponseQuestion> ResponseQuestion { get; set; }
    }
}
