﻿
namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class QuestionOption
    {
        public int Id { get; set; }
        public int OptionId { get; set; }
        public int QuestionId { get; set; }

        public virtual Option Option { get; set; }
        public virtual Question Question { get; set; }
    }
}
