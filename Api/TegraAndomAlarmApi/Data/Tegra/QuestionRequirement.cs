﻿
namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class QuestionRequirement
    {
        public int QuestionId { get; set; }
        public int RequirementId { get; set; }

        public virtual Question Question { get; set; }
        public virtual Requirement Requirement { get; set; }
    }
}
