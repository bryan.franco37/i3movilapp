﻿using System;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class ResponseAssessment
    {
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Crcode { get; set; }
        public string Country { get; set; }
        public string FactoryName { get; set; }
        public string FactoryContactName { get; set; }
        public string FactoryAssessorName { get; set; }
        public string OtherAssessorName { get; set; }

        public virtual Response IdNavigation { get; set; }
    }
}
