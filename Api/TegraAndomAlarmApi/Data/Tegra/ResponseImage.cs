﻿
namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class ResponseImage
    {
        public int Id { get; set; }
        public int? ResponseId { get; set; }
        public int QuestionId { get; set; }
        public string FileName { get; set; }
        public byte[] Image { get; set; }

        public virtual Question Question { get; set; }
        public virtual Response Response { get; set; }
    }
}
