﻿
namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class ResponseQuestionImage
    {
        public int Id { get; set; }
        public int ResponseQuestionId { get; set; }
        public string FileName { get; set; }
        public byte[] Image { get; set; }

        public virtual ResponseQuestion ResponseQuestion { get; set; }
    }
}
