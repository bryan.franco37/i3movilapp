﻿using System.Collections.Generic;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class SubSection
    {
        public SubSection()
        {
            Group = new HashSet<Group>();
        }

        public int Id { get; set; }
        public int? SectionId { get; set; }
        public int? Number { get; set; }
        public string Description { get; set; }

        public virtual Section Section { get; set; }
        public virtual ICollection<Group> Group { get; set; }
    }
}
