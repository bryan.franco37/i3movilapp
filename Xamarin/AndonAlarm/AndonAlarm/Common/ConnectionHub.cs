﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.AspNetCore.SignalR.Client;

namespace AndonAlarm.Common
{
    public class ConnectionHub
    {
        private HubConnection _connection;

        public HubConnection GetConnectionBuilder(string url)
        {
            _connection = new HubConnectionBuilder().WithUrl(url, (options) =>
            {
                options.HttpMessageHandlerFactory = (handler) =>
                {
                    if (handler is HttpClientHandler clientHandler)
                    {
                        clientHandler.ServerCertificateCustomValidationCallback = ValidateCertificate;
                    }
                    return handler;
                };
            }).Build();
            return _connection;
        }

        bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            // TODO: You can do custom validation here, or just return true to always accept the certificate.
            // DO NOT use custom validation logic in a production application as it is insecure.
            return true;
        }
    }
}
