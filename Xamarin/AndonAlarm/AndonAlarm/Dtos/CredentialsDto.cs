﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndonAlarm.Dtos
{
  public class CredentialsDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
