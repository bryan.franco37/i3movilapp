﻿using System;
using System.Collections.Generic;
using System.Text;
using AndonAlarm.Models;

namespace AndonAlarm.Dtos
{
  public class ProductionLineDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Operator { get; set; }
        public AndonAlert Alert { get; set; }

    }
}
