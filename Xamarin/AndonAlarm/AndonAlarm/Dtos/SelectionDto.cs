﻿using AndonAlarm.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AndonAlarm.Dtos
{
    public class SelectionDto
    {
        public List<Factory> Factories { get; set; }
        public List<Process> Processes { get; set; }
    }
}
