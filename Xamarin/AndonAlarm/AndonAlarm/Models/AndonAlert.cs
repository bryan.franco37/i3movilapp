﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndonAlarm.Models
{
   public class AndonAlert
    {
        public int AndonAlertId { get; set; }
        public string UserId { get; set; }
        //public virtual User User { get; set; }
        public int PpmAddressId { get; set; }
        public int AndonAlertTypeId { get; set; }
        public virtual AndonAlertType AndonAlertType { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;


        public int DrawAndonAlertTypeId
        {
            get
            {
                if (AndonAlertTypeId == 2 && (DateTime.UtcNow - CreatedAt).TotalSeconds > 7200)
                {
                    return 1;
                }
                else
                {
                    return AndonAlertTypeId;
                }
            }
        }

        public int AlarmDelaySeconds
        {
            get
            {
                return (int)Math.Round((CreatedAt.AddSeconds(7200) - DateTime.UtcNow).TotalSeconds, 0);
            }
        }

    }
}
