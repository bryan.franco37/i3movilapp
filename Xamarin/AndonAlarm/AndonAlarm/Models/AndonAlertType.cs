﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndonAlarm.Models
{
   public class AndonAlertType
    {
        public int AndonAlertTypeId { get; set; }
        public string Name { get; set; }
    }
}
