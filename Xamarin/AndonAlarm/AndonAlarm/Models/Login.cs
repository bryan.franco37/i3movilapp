﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using AndonAlarm.Views;

namespace AndonAlarm.Models
{
    public  class Login 
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
