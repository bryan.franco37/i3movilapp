﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndonAlarm.Models
{
    public class Process
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
