﻿using System.Net;
using System.Net.Http;
using Xamarin.Forms;

namespace AndonAlarm.Services
{
   public class HttpClientManager
    {
        public static HttpClient GetHttpClient() {            
            switch (Device.RuntimePlatform) {
                case Device.Android:
                    return new HttpClient(DependencyService.Get<Services.IHTTPClientHandlerCreationService>().GetInsecureHandler());
                    break;
                case Device.iOS:
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    return new HttpClient(new HttpClientHandler());
                    break;
                default:
                     return new HttpClient();
                    break;
            }            
        }


    }
}
