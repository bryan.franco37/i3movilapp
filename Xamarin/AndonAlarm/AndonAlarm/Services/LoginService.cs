﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AndonAlarm.Common;
using AndonAlarm.Views;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using AndonAlarm.Dtos;
using AndonAlarm.Models;

namespace AndonAlarm.Services
{
    public class LoginService
    {
        private AuthenticationResult authResult = null;
        private string _clientId;
        private string _authority;
        private string _returnUri;
        private string _graphResourceUri;
        private string _apiUrl;


        public LoginService()
        {
            _clientId = AppSettingsManager.Settings["ClientId"];
            _authority = AppSettingsManager.Settings["Authority"];
            _returnUri = AppSettingsManager.Settings["ReturnUri"]; 
            _graphResourceUri = AppSettingsManager.Settings["GraphResourceUri"];
            _apiUrl = AppSettingsManager.Settings["ApiUrl"];
        }

        public async Task TokenAzureValidation()
        {
            //AZURE TOKEN 
            string tokenAzure = string.Empty;
            if (App.Current.Properties.ContainsKey("TokenAzure"))
            {
                tokenAzure = (string)App.Current.Properties["TokenAzure"];
            }
            else
            {
                tokenAzure = await GetTokenAzure();
            }

            //API TOKEN 
            var tokenApi = await GetTokenAPI($"{_apiUrl}/Api", tokenAzure);
            if (!string.IsNullOrEmpty(tokenApi))
            {
                App.Current.Properties.Add("TokenAPI", tokenApi);
                await App.Current.SavePropertiesAsync();

                var page = new MainPage();
                //await Navigation.PushAsync(page);
            }
        }
        public async Task<string> GetTokenAzure()
        {
            var auth = DependencyService.Get<IAndroidService>();
            authResult = await auth.Authenticate(_authority, _graphResourceUri, _clientId, _returnUri);
            App.Current.Properties.Add("TokenAzure", authResult.AccessToken);
            App.Current.Properties.Add("UserName", authResult.UserInfo.DisplayableId);
            await App.Current.SavePropertiesAsync();

            return authResult.AccessToken;
        }

        public async Task<string> GetTokenAPI(string apiUrl, string username, string password)
        {

            using (var client = HttpClientManager.GetHttpClient())
            {
                var cred = new CredentialsDto()
                {
                    Username = username.Trim(),
                    Password = password.Trim()
                };

                try
                {
                    var response = await client.PostAsJsonAsync($"{apiUrl}/auth/token", cred);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        var jResult = JObject.Parse(content);
                        App.Current.Properties.Add("EmailValidation", false);
                        return jResult["token"].ToString();
                    }
                    else
                    {
                        //JObject json = JObject.Parse(await response.Content.ReadAsStringAsync());
                        await App.Current.MainPage.DisplayAlert("Alert", response.ReasonPhrase, "OK");
                    }
                }
                catch (Exception ex)
               {
                    if (ex.InnerException == null)
                    {
                        await App.Current.MainPage.DisplayAlert("Warning", "Error when try to connect to the server.",
                            "OK");

                    }
                    else
                    {
                        var error = ex.InnerException.Message;
                        if (error.Contains("Error: ConnectFailure (No route to host)"))
                        {
                            await App.Current.MainPage.DisplayAlert("Warning",
                                "An error occours when try to connect to the server (No route to host).", "OK");

                        }

                        if (error.Contains("The operation has timed out."))
                        {
                            await App.Current.MainPage.DisplayAlert("Warning",
                                "The server is taking to much to respond. Try again", "OK");

                        }
                        if (error.Contains("Failed to connect to"))
                        {
                            await App.Current.MainPage.DisplayAlert("Warning",
                                "Autenthication Failed, Your not connected to Tegra Network", "OK");

                        }
                    }
                }
                
                return string.Empty;
            }
        }
        

        public async Task<string> GetTokenAPI(string apiUrl, string tokenAzure)
        {
            //ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
            using (var client = HttpClientManager.GetHttpClient())
            {
                client.Timeout = new TimeSpan(0, 0, 10);
                try
                {
                    var response = await client.PostAsJsonAsync($"{apiUrl}/auth/", tokenAzure);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        var jResult = JObject.Parse(content);
                        App.Current.Properties.Add("AzureValidation", true);
                        return jResult["token"].ToString();
                    }
                    else
                    {
                        JObject json = JObject.Parse(await response.Content.ReadAsStringAsync());
                        await App.Current.MainPage.DisplayAlert("Alert", json["message"].ToString(), "OK");
                    }
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    if (ex.InnerException != null) error += " " + ex.InnerException.Message;
                   await App.Current.MainPage.DisplayAlert("Alert", error, "OK");
                }
                //TegraButton.IsEnabled = true;
                return string.Empty;
            }
        }

    }
}
