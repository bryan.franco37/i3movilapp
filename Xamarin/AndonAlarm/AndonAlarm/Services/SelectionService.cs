﻿using AndonAlarm.Dtos;
using AndonAlarm.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AndonAlarm.Services
{
    public class SelectionService
    {

        //string tokenApi = (string) App.Current.Properties["TokenAPI"];


        public async Task<SelectionDto> GetFactoryAndProcess()
        {
            try
            {
                var apiUrl = AppSettingsManager.Settings["ApiUrl"];
                var client = HttpClientManager.GetHttpClient();
                string tokenApi = string.Empty;
                //if (App.Current.Properties.ContainsKey("TokenAPI"))
                //{
                //    tokenApi = (string)App.Current.Properties["TokenAPI"];
                //}
               // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                var response = await client.GetAsync($"{apiUrl}/andon/GetFactoriesAndProcessesMovil");
                var content = await response.Content.ReadAsStringAsync();
                var list = JsonConvert.DeserializeObject<SelectionDto>(content);
                return list;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
    }
}
