﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AndonAlarm.Common;
using AndonAlarm.Models;
using AndonAlarm.Services;
using AndonAlarm.Views;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace AndonAlarm.ViewModels
{
    public class LoginViewModel
    {
        public  ICommand LoginCommand { get; set; }
        public bool isEnableButton { get; set; }
        public ICommand LoginOfficeCommand { get; set; }
        private INavigation _navigation;
        private LoginService _serviceLogin;
        private Login _login;
        private string _apiUrl;

        public Login LoginModel
        {
            get { return _login; }
            set { _login = value; }
        }

        public bool IsBussy { get; set; }

        

        public LoginViewModel(INavigation navigation)
        {
            IsBussy = false;
            LoginCommand = new Command(Login);
            //LoginOfficeCommand= new Command(LoginOffice);
            _apiUrl = AppSettingsManager.Settings["ApiUrl"];
            _login = new Login
            {
                Username = "said.franco",
                Password = "Franco2019."
            };
            _navigation = navigation;           
            _serviceLogin= new LoginService();
            isEnableButton = false;

        }
         
        private async void Login()
        {
            await PopupNavigation.Instance.PushAsync(new PopupPage());
            //var token = await _serviceLogin.GetTokenAPI($"{_apiUrl}/Api", LoginModel.Username, LoginModel.Password);
            //if (!string.IsNullOrEmpty(token))
            //{
            //    App.Current.Properties.Add("UserName", LoginModel.Username);
            //    App.Current.Properties.Add("TokenAPI", token);

            //}
            await _navigation.PushAsync(new SelectionPage());
            await PopupNavigation.Instance.PopAsync();
        }

        //private async void LoginOffice()
        //{
        //    string tokenAzure = string.Empty;
        //    if (App.Current.Properties.ContainsKey("TokenAzure"))
        //    {
        //        tokenAzure = (string)App.Current.Properties["TokenAzure"];
        //    }
        //    else
        //    {
        //        tokenAzure = await _serviceLogin.GetTokenAzure();
        //    }
        //    var tokenApi = await _serviceLogin.GetTokenAPI($"{_apiUrl}/Api", tokenAzure);
        //    if (!string.IsNullOrEmpty(tokenApi))
        //    {
        //        App.Current.Properties.Add("TokenAPI", tokenApi);
        //        await App.Current.SavePropertiesAsync();

        //        var page = new MainPage();
        //        await _navigation.PushAsync(new MainPage());
        //    }
        //}
    }
}
