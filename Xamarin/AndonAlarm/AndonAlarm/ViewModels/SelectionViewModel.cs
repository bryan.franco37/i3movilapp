﻿using AndonAlarm.Models;
using AndonAlarm.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AndonAlarm.Annotations;
using AndonAlarm.Views;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace AndonAlarm.ViewModels
{
    public class SelectionViewModel : INotifyPropertyChanged
    {
        private readonly SelectionService _selectionService;
        public ICommand ShowAndonCommand { get; set; }
        private List<Factory> _lstFactories;
        public List<Factory> LstFactories
        {
            get => _lstFactories;
            set
            {
                if (Equals(value, _lstFactories)) return;
                _lstFactories = value;
                OnPropertyChanged();
            }
        }
       
        private Factory _selectedFactory;
        public Factory SelectedFactory
        {
            get => _selectedFactory;
            set
            {
                if (Equals(value, _selectedFactory)) return;
                _selectedFactory = value;
                OnPropertyChanged();
                ValidateButtonState();

            }
        }
        
        private List<Process> _lstProcess;
        public List<Process> LstProcess
        {
            get => _lstProcess;
            set
            {
                if (Equals(value, _lstProcess)) return;
                _lstProcess = value;
                OnPropertyChanged();
            }
        }
        
        public Process SelectedProcess
        {
            get => _selectedProcess;
            set
            {
                if (Equals(value, _selectedProcess)) return;
                _selectedProcess = value;
                OnPropertyChanged();
                ValidateButtonState();
            }
        }
        private Process _selectedProcess;
        
        private bool _buttonState;
        public bool ButtonState
        {
            get => _buttonState;
            set
            {
                if (value == _buttonState) return;
                _buttonState = value;
                OnPropertyChanged();
            }
        }
        
        private readonly INavigation _navigation;


        public SelectionViewModel(INavigation navigation)
        {
            ShowAndonCommand = new Command(ShowAndon);
            _lstFactories = new List<Factory>();
            _lstProcess = new List<Process>();
            _selectionService = new SelectionService();
            _navigation = navigation;
            ButtonState = false;
        }

        public async Task FillFactoriesAndProcess()
        {
            var lst = await _selectionService.GetFactoryAndProcess();
            ButtonState = false;
            LstFactories =  lst.Factories;
            LstProcess = lst.Processes;
        }

        private  void ShowAndon()
        {
             PopupNavigation.Instance.PushAsync(new PopupPage());
            ////Used when press back 
            if (Application.Current.Properties.ContainsKey("FactoryId") && Application.Current.Properties.ContainsKey("ProcessId"))
            {
                Application.Current.Properties.Remove("FactoryId");
                Application.Current.Properties.Remove("ProcessId");
                Application.Current.Properties.Remove("FactoryName");
                Application.Current.Properties.Remove("ProcessName");
            }
            Application.Current.Properties.Add("FactoryId", SelectedFactory.Id);
            Application.Current.Properties.Add("ProcessId", SelectedProcess.Id);
            Application.Current.Properties.Add("FactoryName", SelectedFactory.Name);
            Application.Current.Properties.Add("ProcessName", SelectedProcess.Name);
            _navigation.PushAsync(new MainPage());
             PopupNavigation.Instance.PopAsync();

        }

        private void ValidateButtonState()
        {
            if (SelectedProcess !=null && SelectedFactory !=null)
            {
                ButtonState = true;
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
