﻿using AndonAlarm.Common;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AndonAlarm.Dtos;
using AndonAlarm.Models;
using AndonAlarm.Services;

namespace AndonAlarm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlertPage
    {
        private ProductionLineDto ProductionLineDto { get; set; }
        private string _apiUrl;

        public AlertPage(ProductionLineDto productionLineDto)
        {
            InitializeComponent();
            _apiUrl = AppSettingsManager.Settings["ApiUrl"];
            ProductionLineDto = productionLineDto;

            if (ProductionLineDto.Alert == null)
            {
                Comment.IsVisible = true;
                CommentLabel.IsVisible = true;
                AlertButton.IsVisible = true;
                WarningButton.IsVisible = true;
                CompleteButton.IsVisible = false;
            }
            else if (ProductionLineDto.Alert.DrawAndonAlertTypeId == 1)
            {
                Comment.IsVisible = false;
                CommentLabel.IsVisible = false;
                AlertButton.IsVisible = false;
                WarningButton.IsVisible = false;
                CompleteButton.IsVisible = true;
                WorklessButton.IsVisible = false;
            }
            else if (ProductionLineDto.Alert.DrawAndonAlertTypeId == 2)
            {
                Comment.IsVisible = false;
                CommentLabel.IsVisible = false;
                AlertButton.IsVisible = true;
                WarningButton.IsVisible = false;
                CompleteButton.IsVisible = true;
                WorklessButton.IsVisible = false;
            }
            else if (ProductionLineDto.Alert.DrawAndonAlertTypeId == 3)
            {
                Comment.IsVisible = false;
                CommentLabel.IsVisible = false;
                AlertButton.IsVisible = false;
                WarningButton.IsVisible = false;
                CompleteButton.IsVisible = true;
                WorklessButton.IsVisible = false;
            }
        }

        private async void AlertButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                AlertButton.IsEnabled = false;
                var alert = new AlertDto
                {
                    FactoryId = int.Parse(Application.Current.Properties["FactoryId"].ToString()),
                    ProcessId = int.Parse(Application.Current.Properties["ProcessId"].ToString()),
                    ProductionLineId = ProductionLineDto.Id,
                    TypeId = 1,
                    Comment = Comment.Text
                };
                using (var client = HttpClientManager.GetHttpClient())
                {
                    string tokenApi = string.Empty;
                    if (App.Current.Properties.ContainsKey("TokenAPI"))
                    {
                        tokenApi = (string) App.Current.Properties["TokenAPI"];
                    }

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                    var res = await client.PostAsJsonAsync($"{_apiUrl}/andon/CreateAlertMovil", alert);
                    if (res.IsSuccessStatusCode)
                    {
                        AlertButton.IsEnabled = true;
                    }
                    else
                    {
                        MessagingCenter.Send(new Message {Value = false}, "ValidToken");
                    }
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
                MessagingCenter.Send(new Message {Value = false}, "ValidToken");
            }

            MessagingCenter.Send(new Message {Value = true}, "Enabled");
            await PopupNavigation.Instance.PopAsync();
        }

        private async void WorklessButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                WorklessButton.IsEnabled = false;
                var alert = new AlertDto
                {
                    FactoryId = int.Parse(Application.Current.Properties["FactoryId"].ToString()),
                    ProcessId = int.Parse(Application.Current.Properties["ProcessId"].ToString()),
                    ProductionLineId = ProductionLineDto.Id,
                    TypeId = 3,
                    Comment = Comment.Text
                };
                using (var client = HttpClientManager.GetHttpClient())
                {
                    string tokenApi = string.Empty;
                    if (App.Current.Properties.ContainsKey("TokenAPI"))
                    {
                        tokenApi = (string)App.Current.Properties["TokenAPI"];
                    }

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                    var res = await client.PostAsJsonAsync($"{_apiUrl}/andon/CreateAlertMovil", alert);
                    if (res.IsSuccessStatusCode)
                    {
                        AlertButton.IsEnabled = true;
                    }
                    else
                    {
                        MessagingCenter.Send(new Message { Value = false }, "ValidToken");
                    }
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
                MessagingCenter.Send(new Message { Value = false }, "ValidToken");
            }

            MessagingCenter.Send(new Message { Value = true }, "Enabled");
            await PopupNavigation.Instance.PopAsync();
        }

        private async void WarningButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                WarningButton.IsEnabled = false;
                var alert = new AlertDto
                {
                    FactoryId = int.Parse(Application.Current.Properties["FactoryId"].ToString()),
                    ProcessId = int.Parse(Application.Current.Properties["ProcessId"].ToString()),
                    ProductionLineId = ProductionLineDto.Id,
                    TypeId = 2,
                    Comment = Comment.Text
                };
                using (var client = HttpClientManager.GetHttpClient())
                {
                    string tokenApi = string.Empty;
                    if (App.Current.Properties.ContainsKey("TokenAPI"))
                    {
                        tokenApi = (string) App.Current.Properties["TokenAPI"];
                    }

                    client.Timeout = new TimeSpan(0, 0, 10);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                    var res = await client.PostAsJsonAsync($"{_apiUrl}/andon/CreateAlertMovil", alert);
                    if (res.IsSuccessStatusCode)
                    {
                        WarningButton.IsEnabled = true;
                    }
                    else
                    {
                        MessagingCenter.Send(new Message {Value = false}, "ValidToken");
                    }
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
                MessagingCenter.Send(new Message {Value = false}, "ValidToken");
            }

            MessagingCenter.Send(new Message {Value = true}, "Enabled");
            await PopupNavigation.Instance.PopAsync();
        }

        private async void CompleteButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                CompleteButton.IsEnabled = false;
                using (var client = HttpClientManager.GetHttpClient())
                {
                    string tokenApi = string.Empty;
                    if (App.Current.Properties.ContainsKey("TokenAPI"))
                    {
                        tokenApi = (string) App.Current.Properties["TokenAPI"];
                    }

                    client.Timeout = new TimeSpan(0, 0, 40);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                    var res = await client.DeleteAsync(
                        $"{_apiUrl}/andon/RemoveAlertMovil/{ProductionLineDto.Alert.AndonAlertId}");

                    CompleteButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
                MessagingCenter.Send(new Message {Value = false}, "ValidToken");
            }

            MessagingCenter.Send(new Message {Value = true}, "Enabled");
            await PopupNavigation.Instance.PopAsync();
        }

        protected override bool OnBackgroundClicked()
        {
            MessagingCenter.Send(new Message {Value = true}, "Enabled");
            return base.OnBackgroundClicked();
        }
    }
}