﻿using AndonAlarm.ViewModels;
using Xamarin.Forms;

namespace AndonAlarm.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            UserValidation.IsVisible = false;
            PassValidation.IsVisible = false;
            LoginButton.BorderColor = Color.LightSlateGray;
            LoginButton.BackgroundColor = Color.LightSlateGray; NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new LoginViewModel(Navigation);
            LoginButton.IsEnabled = false;
            if (!string.IsNullOrEmpty(PasswordEditor.Text) && !string.IsNullOrEmpty(UsernameEditor.Text))
            {
                LoginButton.IsEnabled = true;
                LoginButton.BorderColor = Color.Blue;
                LoginButton.BackgroundColor = Color.Blue;
            }
        }

        private void UsernameEditor_OnUnfocused(object sender, FocusEventArgs e)
        {
            if (string.IsNullOrEmpty(UsernameEditor.Text))
            {
                UserValidation.IsVisible = true;
                LoginButton.IsEnabled = false;
                LoginButton.BorderColor = Color.LightSlateGray;
                LoginButton.BackgroundColor = Color.LightSlateGray;
            }
            else
            {
                UserValidation.IsVisible = false;
                if (!string.IsNullOrEmpty(PasswordEditor.Text) && !string.IsNullOrEmpty(UsernameEditor.Text))
                {
                    LoginButton.IsEnabled = true;
                    LoginButton.BorderColor = Color.Blue;
                    LoginButton.BackgroundColor = Color.Blue;
                }
            }
        }
        
        private void PasswordEditor_OnUnfocused(object sender, FocusEventArgs e)
        {
            if (string.IsNullOrEmpty(PasswordEditor.Text))
            {
                PassValidation.IsVisible = true;
                LoginButton.IsEnabled = false;
                LoginButton.BorderColor = Color.LightSlateGray;
                LoginButton.BackgroundColor = Color.LightSlateGray;
            }
            else
            {
                PassValidation.IsVisible = false;
                if (!string.IsNullOrEmpty(PasswordEditor.Text)  && !string.IsNullOrEmpty(UsernameEditor.Text)) 
                {
                    LoginButton.IsEnabled = true;
                    LoginButton.BorderColor = Color.Blue;
                    LoginButton.BackgroundColor = Color.Blue;
                }

            }
            
        }

    }
}
