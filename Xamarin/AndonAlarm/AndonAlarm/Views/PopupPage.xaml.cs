﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Contracts;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AndonAlarm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupPage : IPopupNavigation
    {
        public PopupPage()
        {
            InitializeComponent();
        }

        public Task PushAsync(Rg.Plugins.Popup.Pages.PopupPage page, bool animate = true)
        {
            throw new NotImplementedException();
        }

        public Task PopAsync(bool animate = true)
        {
            throw new NotImplementedException();
        }

        public Task PopAllAsync(bool animate = true)
        {
            throw new NotImplementedException();
        }

        public Task RemovePageAsync(Rg.Plugins.Popup.Pages.PopupPage page, bool animate = true)
        {
            throw new NotImplementedException();
        }

        public IReadOnlyList<Rg.Plugins.Popup.Pages.PopupPage> PopupStack { get; }
    }
}