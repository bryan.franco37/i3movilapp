﻿using AndonAlarm.Services;
using AndonAlarm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AndonAlarm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectionPage : ContentPage
    {
        public SelectionPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new SelectionViewModel(Navigation);
        }
        protected override bool OnBackButtonPressed() => true;

        protected override async void OnAppearing()
        {
            await ((SelectionViewModel)BindingContext).FillFactoriesAndProcess();
        }
    }
}